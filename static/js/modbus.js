$(document).ready(function() {
  //connect to the socket server.
  var socket = io.connect('http://' + document.domain + ':' + location.port + '/modbus');
  socket.emit('get_connection_status');

  socket.on('register_error', function() {
    $("#registers").text("Unvalid register");
  });

  $("#modbus_connect").click(function() {
    socket.emit('connect_modbus', {
      ip: $("#ip_addr").val(),
      port: $("#port").val()
    });
  });

  $("#modbus_disconnect").click(function() {
    $("#registers").text("");
    socket.emit('disconnect_modbus');
  });

  socket.on('registers', function(msg) {
    var data_raw = msg.data.raw;
    var data_sint16 = msg.data.sint16;
    var data_uint16 = msg.data.uint16;
    var data_uint32 = msg.data.uint32;
    var data_sint32 = msg.data.sint32;
    var data_float32 = msg.data.float32;
    var data_uint64 = msg.data.uint64;
    var data_sint64 = msg.data.sint64;
    var data_float64 = msg.data.float64;

    $("#tbodyid").empty();

    for (i = 0; i < msg.data.raw.length; i++) {
      $('#tbodyid').append('<tr> <td>' + i + '</td> <td>' + data_raw[i] + '</td>  <td>' + data_uint16[i] + '</td> <td>' + data_sint16[i] + '</td>  <td>' + data_uint32[i] + '</td>  <td>' + data_sint32[i] + '</td>  <td>' + data_float32[i] + '</td>  <td>' + data_uint64[i] + '</td>  <td>' + data_sint64[i] + '</td>  <td>' + data_float64[i] + '</td></tr>');

    }

  });
  socket.on('connection_status', function(msg) {
    if (msg.data == true) {
      $("#connection_status").text("Connected");
    }
    if (msg.data == false) {
      $("#connection_status").text("Disconnected");
    }


  });
  socket.on('clear_register_table', function(msg) {
    $("#tbodyid").empty();
  });

  $("#start_read_modbus").click(function() {

    var endian = $('#endian').val();
    var register_type = $('#registry_type').val();

    socket.emit('start_read_modbus', {
      start_addr: $("#start_addr").val(),
      length: $("#read_length").val(),
      pull_intervall: $("#pull_interval").val(),
      node_id: $("#node_id").val(),
      endian: endian,
      register_type: register_type
    });
  });

  $("#stop_read_modbus").click(function() {
    $("#registers").text("");
    socket.emit('stop_read_modbus');
  });



});
