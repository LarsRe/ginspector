$(document).ready(function() {
  //connect to the socket server.
  var socket = io.connect('http://' + document.domain + ':' + location.port + '/mqtt');

  socket.emit('get_connection_status');


  $("#mqtt_connect").click(function() {
    socket.emit('connect_mqtt', {
      ip: $("#ip_addr").val(),
      port: $("#port").val()
    });
  });

  $("#mqtt_disconnect").click(function() {
    socket.emit('disconnect_mqtt');
  });

  socket.on('connection_status', function(msg) {
    if (msg.data == true) {
      $("#mqtt_connection_status").text("Connected");
    }
    if (msg.data == false) {
      $("#mqtt_connection_status").text("Disconnected");
    }
  });
  $("#mqtt_publish").click(function() {
    socket.emit('publish_mqtt', {
      topic: $("#topic").val(),
      message: $("#message").val()
    });
  });

  $("#mqtt_subscribe").click(function() {
    socket.emit('subscribe_mqtt', {
      topic: $("#topic_subscribe").val()
    });
  });
  socket.on('message', function(msg) {
    console.log(msg);
  });



});
