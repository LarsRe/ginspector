import paho.mqtt.client as mqtt #import the client1
import logging


class Mqtt:

    def __init__(self, ip="127.0.0.1", port=1883):
        self.logger = logging.getLogger(__name__)
        self.ip = ip
        self.port = port
        self.connection_status = False
        self.client = mqtt.Client("P1") #create new instance

    def connect(self):
        #client.username_pw_set(username="steve",password="password")
        #client1.tls_set("/home/km/mosquitto/tls/ca.crt")

        self.connection_status = self.client.connect(self.ip, port=self.port)

        if self.connection_status == 0:
            self.connection_status = True
            print("connected to client")
        else:
            print(f"Connection failed with error code {self.connection_status}")

        return self.connection_status

    def disconnect(self):
        self.connection_status = self.client.disconnect()

        if self.connection_status == 0:
            self.connection_status = False
            print("Disconnected from client")
        else:
            print(f"Disconnection failed with error code {self.connection_status}")

        return self.connection_status

    def publish(self, topic, message):
        self.client.publish(topic, message)

    def subscribe(self, topic):
        self.client.subscribe(topic)

    def on_message(self, client1, userdata, message):
        value = float(message.payload.decode("utf-8"))
        measurement_tmp = message.topic.split("/")
        measurement = "/".join(measurement_tmp[:-1])
        field_name = measurement_tmp[-1]

#on_unsubscribe(client, userdata, mid)

if __name__ == '__main__':
    mqtt = Mqtt()
    mqtt.connect()
    mqtt.publish("test/123", "OFF2")
    mqtt.disconnect()
    mqtt.publish("test/123", "OFF3")

    
