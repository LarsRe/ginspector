from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian
import logging
import math

class Modbus:
    def __init__(self, ip="127.0.0.1", port=502, pullrate=0.5, timeout=1):
        self.logger = logging.getLogger(__name__)
        self.ip = ip
        self.port = port
        self.pullrate = pullrate
        self.timeout = timeout
        self.connection_status = False
        self.device_connection = None

    def connect(self):
        """
        Method connects to modbus and stores the connection object in self.device_connection for further use.
        Return True og False depending on connnection status
        """
        self.device_connection = ModbusClient(self.ip, timeout=self.timeout, port=self.port)
        self.connection_status = self.device_connection.connect()

        return self.connection_status

    def disconnect(self):
        self.device_connection.close()
        #self.device_connection = None
        self.connection_status = False
        return self.connection_status

    def read_holding_register(self, start_addr, register_length, unit_id):
        register = self.device_connection.read_holding_registers(
            start_addr, register_length, unit=unit_id)

        return register

    def read_input_register(self, start_addr, register_length, unit_id):
        register = self.device_connection.read_input_registers(
            start_addr, register_length, unit=unit_id)

        return register

    def read_coil_register(self, start_addr, register_length, unit_id):
        register = self.device_connection.read_coils(
            start_addr, register_length, unit=unit_id)

        return register

    def decode(self, register, counter, byte_type, endian='big', address=None, bit_nr=None):
        try:
            if byte_type == 'UINT16':
                temparr = [register.registers[counter]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_16bit_uint()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_16bit_uint()
                counter += 1

            if byte_type == 'SINT16':
                temparr = [register.registers[counter]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_16bit_int()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_16bit_int()

                counter += 1

            if byte_type == 'UINT32':
                temparr = [register.registers[counter],
                           register.registers[counter+1]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_32bit_uint()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_32bit_uint()
                counter += 2

            if byte_type == 'SINT32':
                temparr = [register.registers[counter],
                           register.registers[counter + 1]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_32bit_int()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_32bit_int()
                counter += 2

            if byte_type == 'SINT32-SW':
                temparr = [register.registers[counter + 1],
                           register.registers[counter]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_32bit_int()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_32bit_int()
                counter += 2

            if byte_type == 'FLOAT32':
                temparr = [register.registers[counter],
                           register.registers[counter + 1]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_32bit_float()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_32bit_float()
                counter += 2

            if byte_type == 'BIT16':
                if bit_nr > self.previous_bitnr and self.previous_bitnr != -1:
                    tmp_bit_nr = (bit_nr * -1) - 1
                    tmp_value = int(self.temp_bit_register[tmp_bit_nr])
                    self.previous_bitnr = bit_nr

                else:
                    if endian == 'big':
                        temparr = [register.registers[counter]]
                        decoder = BinaryPayloadDecoder.fromRegisters(
                            temparr, Endian.Big)
                        tmp_value = decoder.decode_16bit_int()

                        if tmp_value < 0:
                            self.temp_bit_register = bin(
                                tmp_value + (1 << 16))[2:].zfill(16)
                        else:
                            self.temp_bit_register = bin(
                                tmp_value)[2:].zfill(16)

                        tmp_bit_nr = (bit_nr * -1) - 1
                        tmp_value = int(self.temp_bit_register[tmp_bit_nr])
                        self.previous_bitnr = bit_nr

                    if endian == 'little':
                        print("Endian little ikke implementert på bit16 decoding")

                    counter += 1

            if byte_type == 'UINT64':
                temparr = [register.registers[counter],
                           register.registers[counter + 1], register.registers[counter + 2], register.registers[counter + 3]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_64bit_uint()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_64bit_uint()
                counter += 4

            if byte_type == 'SINT64':
                temparr = [register.registers[counter],
                           register.registers[counter + 1], register.registers[counter + 2], register.registers[counter + 3]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_64bit_int()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_64bit_int()
                counter += 4

            if byte_type == 'FLOAT64':
                temparr = [register.registers[counter],
                           register.registers[counter + 1], register.registers[counter + 2], register.registers[counter + 3]]
                if endian == 'big':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Big)
                    tmp_value = decoder.decode_64bit_float()
                if endian == 'little':
                    decoder = BinaryPayloadDecoder.fromRegisters(
                        temparr, Endian.Little)
                    tmp_value = decoder.decode_64bit_float()
                counter += 4

            if byte_type == 'DATETIME':
                tmp_value = 'na'
                counter += 4



            if byte_type == 'DUMMY16':
                tmp_value = 'DUMMY'
                counter += 1
            if byte_type == 'DUMMY32':
                tmp_value = 'DUMMY'
                counter += 2
        except:
            self.logger.error(' Failed decoding value ' +
                         register[counter] + ' from device: ' + self.device_name + ', on IP: ' + str(self.ip))
            tmp_value = 'f'
            counter = 'f'

        if isinstance(tmp_value, (float, int)):
            if math.isnan(tmp_value):
                tmp_value = 'f'
            return tmp_value, counter

        else:
            tmp_value = 'f'
            return tmp_value, counter
