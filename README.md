## Ginspector
---
![picture](img/ginspector.png)
The Gadget Inspector or Ginspector for short is a debug tool for industrial automation equipment. It is used to view values and find correct datatypes etc, during troubleshooting and commissioning. The following protocols will be supported by the tool.
- Modbus TCP/IP (Implemented)
- Bacnet IP (Not implemented)
- MQTT (Not implemented)
- SNMP (Not implemented)
- WEB (REST) (Not implemented)

It is built by using Flask with socketio as a data transfer between backend and frontend.

## Contents
- [Usage](#Usage)
  - [Build](#Configuration)
  - [Configuration](#Configuration)
  - [Run](#Run)
- [Issues](#Support)
- [Changelog](#Changelog)
- [License](#License)

## Usage
### Build
To build the docker container from the Dockerfile use the command ```docker build -t gapitio/ginspector:1.1 -t gapitio/ginspector:latest .```

### Configuration
All configuration is done in the web page.

### Run
Ginspector can be started with a local install, through a docker RUN command or through docker-compose.

## Local install
```
pip install -r requirements.txt
python application.py
```

## Docker run
Run as docker using the docker run command
```
docker run -d -p 5000:5000 --restart always --name Ginspector  gapitio/ginspector:1.1
```

## Docker compose
Use the supplied docker-compose.yml
``` docker-compose up -d
```

## Issues
Issues can be logged to the project repo: https://gitlab.com/gapit/ginspector

## Changelog

### v1.1
  * Updated layout
  * Added possibility to change register type
  * Added picture to readme file

### v1.0
  * Initial release

## License
Copyright 2018 Gapit.io

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
