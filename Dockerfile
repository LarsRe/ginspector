FROM python:3.7-alpine3.7

RUN mkdir app

COPY /static /app/static
COPY /templates /app/templates
COPY application.py /app
COPY README.md /app
COPY modbus.py /app
COPY mqtt.py /app
COPY LICENSE.txt /app
COPY requirements.txt /app

WORKDIR /app

RUN apk add --update build-base \
  && pip install -r requirements.txt \
  && apk del build-base

EXPOSE 5000

CMD ["python", "application.py"]
