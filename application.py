"""
Ginspector
"""

# Start with a basic flask app webpage.
import time
import logging
from threading import Thread, Event

from flask_socketio import SocketIO, emit
from flask import Flask, render_template, url_for, copy_current_request_context
from random import random

from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.compat import iteritems
from pymodbus.constants import Endian
from collections import OrderedDict

from modbus import Modbus
from mqtt import Mqtt
logger = logging.getLogger()
logger.setLevel(30)

logger_handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(name)s - %(message)s')
logger_handler.setFormatter(formatter)

logger.addHandler(logger_handler)

__author__ = 'Mjos'

app = Flask(__name__, static_url_path='/static')
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

# turn the flask app into a socketio app
socketio = SocketIO(app)
modbus_connection = Modbus()
mqtt_connection = Mqtt()

thread = Thread()
thread_stop_event = Event()

thread_mqtt = Thread()
mqtt_thread_stop_event = Event()

class InputMqtt(Thread):
    def __init__(self):
        super(InputMqtt, self).__init__()

    def read_subscription(self):
        client1.on_message = on_message
        while not mqtt_thread_stop_event.isSet():
            #client1.loop_start()
            client.loop()
            time.sleep(0.5)

    def run(self):
        self.read_subscription()

class InputModbus(Thread):
    def __init__(self):
        self.delay = 1
        self.address = 1
        self.length = 1
        self.unit_id = 1
        self.pull_intervall = 0.5
        self.endian = "big"
        self.register_type = "holding"
        super(InputModbus, self).__init__()

    def read_values(self, address, length, unit_id, endian, register_type):
        """
        Generate a random number every 1 second and emit to a socketio instance (broadcast)
        Ideally to be run in a separate thread?
        """

        while not thread_stop_event.isSet():

            if register_type == 'holding':
                reg = modbus_connection.read_holding_register(
                    address, length, unit_id)

            if register_type == 'input':
                reg = modbus_connection.read_input_register(
                    address, length, unit_id)

            if register_type == 'coil':
                reg = modbus_connection.read_coil_register(
                    address, length, unit_id)

            try:
                raw_array = reg.registers

            except:
                socketio.emit('register_error', namespace='/modbus')
                return

            uint16_array = []
            sint16_array = []
            uint32_array = []
            sint32_array = []
            float32_array = []
            uint64_array = []
            sint64_array = []
            float64_array = []

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                tmp_value, counter = modbus_connection.decode(
                    reg, counter, 'UINT16', endian)
                uint16_array.append(tmp_value)

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                tmp_value, counter = modbus_connection.decode(
                    reg, counter, 'SINT16', endian)
                sint16_array.append(tmp_value)

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                if counter + 1 >= len(reg.registers):
                    uint32_array.append('-')
                    break
                else:
                    tmp_value, counter = modbus_connection.decode(
                    reg, counter, 'UINT32', endian)
                    uint32_array.append(tmp_value)
                    uint32_array.append('-')

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                if counter + 1 >= len(reg.registers):
                    sint32_array.append('-')
                    break
                else:
                    tmp_value, counter = modbus_connection.decode(
                        reg, counter, 'SINT32', endian)
                    sint32_array.append(tmp_value)
                    sint32_array.append('-')

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                if counter + 1 >= len(reg.registers):
                    float32_array.append('-')
                    break
                else:
                    tmp_value, counter = modbus_connection.decode(
                        reg, counter, 'FLOAT32', endian)
                    float32_array.append(tmp_value)
                    float32_array.append('-')

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                if counter + 3 >= len(reg.registers):
                    uint64_array.append('-')
                    uint64_array.append('-')
                    uint64_array.append('-')
                    break
                else:
                    tmp_value, counter = modbus_connection.decode(
                        reg, counter, 'UINT64', endian)
                    uint64_array.append(tmp_value)
                    uint64_array.append('-')
                    uint64_array.append('-')
                    uint64_array.append('-')

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                if counter + 3 >= len(reg.registers):
                    sint64_array.append('-')
                    sint64_array.append('-')
                    sint64_array.append('-')
                    break
                else:
                    tmp_value, counter = modbus_connection.decode(
                        reg, counter, 'SINT64', endian)
                    sint64_array.append(tmp_value)
                    sint64_array.append('-')
                    sint64_array.append('-')
                    sint64_array.append('-')

            counter = 0
            tmp_value = 0
            while counter < len(reg.registers):
                if counter + 3 >= len(reg.registers):
                    float64_array.append('-')
                    float64_array.append('-')
                    float64_array.append('-')
                    break
                else:
                    tmp_value, counter = modbus_connection.decode(
                        reg, counter, 'FLOAT64', endian)
                    float64_array.append(tmp_value)
                    float64_array.append('-')
                    float64_array.append('-')
                    float64_array.append('-')

        #    stt = decoder.decode_16bit_int()
        #    decoded = OrderedDict([
        #    ('string', decoder.decode_string(8)),
        #    ('bits', decoder.decode_bits()),
        #    ('8int', decoder.decode_8bit_int()),
        #    ('8uint', decoder.decode_8bit_uint()),
        ##   ('16int', decoder.decode_16bit_int()),
        ##   ('16uint', decoder.decode_16bit_uint()),
        ##   ('32int', decoder.decode_32bit_int()),
        ##   ('32uint', decoder.decode_32bit_uint()),
        ##    ('32float', decoder.decode_32bit_float()),
        ##   ('64int', decoder.decode_64bit_int()),
        ##   ('64uint', decoder.decode_64bit_uint()),
        #    ('ignore', decoder.skip_bytes(8)),
        ##    ('64float', decoder.decode_64bit_float())

            obj = {"raw": raw_array, "uint16": uint16_array, "sint16": sint16_array, "uint32": uint32_array, "sint32": sint32_array,
                   "float32": float32_array, "uint64": uint64_array, "sint64": sint64_array, "float64": float64_array}

            socketio.emit('registers', {'data': obj}, namespace='/modbus')
            time.sleep(self.pull_intervall)

    def run(self):
        self.read_values(self.address, self.length, self.unit_id, self.endian, self.register_type)


@app.route('/')
def index():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('index.html')

@app.route('/modbus/read')
def modbus_read():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('modbus_read.html')

@app.route('/modbus/write')
def modbus_write():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('modbus_write.html')

@app.route('/mqtt/publish')
def mqtt_publish():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('mqtt_publish.html')

@app.route('/mqtt/subscribe')
def mqtt_subscribe():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('mqtt_subscribe.html')

@app.route('/mqtt/unsubscribe')
def mqtt_unsubscribe():
    # only by sending this page first will the client be connected to the socketio instance
    client1.unsubscribe(topic)
    #return render_template('mqtt_subscribe.html')



@app.route('/snmp')
def snmp():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('snmp.html')

@app.route('/restapi')
def restapi():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('restapi.html')

@app.route('/bacnet')
def bacnet():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('bacnet.html')

@app.route('/opcua')
def opcua():
    # only by sending this page first will the client be connected to the socketio instance
    return render_template('opcua.html')

#@socketio.on('connect', namespace='/modbus')
# def test_connect():
    #print('Client connected')

#@socketio.on('disconnect', namespace='/modbus')
# def test_disconnect():
#    print('Client disconnected')


@socketio.on('connect_modbus', namespace='/modbus')
def connect_modbus(message):
    modbus_connection.ip = message['ip']
    modbus_connection.port = message['port']
    connection_status = modbus_connection.connect()
    socketio.emit('connection_status', {
                  'data': connection_status}, namespace='/modbus')


@socketio.on('disconnect_modbus', namespace='/modbus')
def disconnect_modbus():
    global thread
    connection_status = modbus_connection.disconnect()
    if thread.isAlive():
        thread_stop_event.set()
    socketio.emit('clear_register_table', {'data': "clear"}, namespace='/modbus')
    socketio.emit('connection_status', {
                  'data': connection_status}, namespace='/modbus')


@socketio.on('start_read_modbus', namespace='/modbus')
def start_read_modbus(message):
    global thread

    if modbus_connection.connection_status is not False:
        thread_stop_event.clear()
        # Start the thread only if the thread has not been started before.
        if not thread.isAlive():
            thread = InputModbus()
            thread.address = int(message['start_addr'])
            thread.length = int(message['length'])
            thread.unit_id = int(message['node_id'])
            thread.pull_intervall = float(message['pull_intervall'])
            thread.endian = message['endian']
            thread.register_type = message['register_type']
            thread.start()


@socketio.on('stop_read_modbus', namespace='/modbus')
def stop_read_modbus():
    global thread
    if thread.isAlive():
        thread_stop_event.set()
    socketio.emit('clear_register_table', {'data': "clear"}, namespace='/modbus')


@socketio.on('get_connection_status', namespace='/modbus')
def get_connection_status():
    connection_status = modbus_connection.connection_status
    socketio.emit('connection_status', {'data': connection_status}, namespace='/modbus')

@socketio.on('get_connection_status', namespace='/mqtt')
def get_mqtt_connection_status():
    connection_status = mqtt_connection.connection_status
    socketio.emit('connection_status', {'data': connection_status}, namespace='/mqtt')


@socketio.on('connect_mqtt', namespace='/mqtt')
def connect_modbus(message):
    mqtt_connection.ip = message['ip']
    mqtt_connection.port = int(message['port'])
    connection_status = mqtt_connection.connect()
    socketio.emit('connection_status', {
                  'data': connection_status}, namespace='/mqtt')

@socketio.on('disconnect_mqtt', namespace='/mqtt')
def disconnect_mqtt():
    connection_status = mqtt_connection.disconnect()
    socketio.emit('connection_status', {
                  'data': connection_status}, namespace='/mqtt')

@socketio.on('publish_mqtt', namespace='/mqtt')
def publish_mqtt(message):
    print(message['topic'])
    print(message['message'])
    mqtt_connection.publish(message['topic'], message['message'])

@socketio.on('subscribe_mqtt', namespace='/mqtt')
def subscribe_mqtt(message):
    mqtt_connection.subscribe(message['topic'])
    global thread_mqtt

    if mqtt_connection.connection_status is not False:
        thread_stop_event.clear()
        # Start the thread only if the thread has not been started before.
        if not thread_mqtt.isAlive():
            thread_mqtt = InputMqtt()
            thread_mqtt.start()


if __name__ == '__main__':
    socketio.run(app, debug=True, host='0.0.0.0')
